package ir.netbox.audition.view.presenter

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.ViewGroup
import androidx.leanback.widget.ImageCardView
import androidx.leanback.widget.ImageCardView.CARD_TYPE_FLAG_IMAGE_ONLY
import androidx.leanback.widget.Presenter
import com.bumptech.glide.Glide
import ir.netbox.audition.data.model.ContentX

/**
 * A TopicPresenter is used to generate Views and bind Objects to them on demand.
 * It contains an ImageCardView.
 */
class TopicPresenter : Presenter() {
    private var mDefaultCardImage: Drawable? = null

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        val cardView = object : ImageCardView(parent.context) {
        }
        cardView.cardType = CARD_TYPE_FLAG_IMAGE_ONLY
        cardView.isFocusable = true
        cardView.isFocusableInTouchMode = true
        return ViewHolder(cardView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, item: Any) {
        val content = item as ContentX
        val cardView = viewHolder.view as ImageCardView

        if (content.poster != null) {
            cardView.setMainImageDimensions(400, 150)

            Glide.with(viewHolder.view.context)
                .load(content.poster)
                .centerCrop()
                .error(mDefaultCardImage)
                .into(cardView.mainImageView)
        }
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder) {
        Log.d(TAG, "onUnbindViewHolder")
        val cardView = viewHolder.view as ImageCardView
        // Remove references to images so that the garbage collector can free up memory
        cardView.badgeImage = null
        cardView.mainImage = null
    }

    companion object {
        private val TAG = "CardPresenter"
    }
}