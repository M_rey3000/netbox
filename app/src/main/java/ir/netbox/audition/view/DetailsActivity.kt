package ir.netbox.audition.view

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import ir.netbox.audition.R

/**
 * Details activity class that loads [MovieDetailsFragment] class.
 */
class DetailsActivity : FragmentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.details_fragment, MovieDetailsFragment())
                .commitNow()
        }
    }

    companion object {
        const val MOVIE_KEY = "movie"
        const val SHARED_ELEMENT_NAME = "element"
    }

}