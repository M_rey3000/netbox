package ir.netbox.audition.view


import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.leanback.app.DetailsSupportFragment
import androidx.leanback.app.DetailsSupportFragmentBackgroundController
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import ir.netbox.audition.R
import ir.netbox.audition.data.api.MovieService
import ir.netbox.audition.data.api.RetrofitInstance
import ir.netbox.audition.data.model.MovieDetail
import ir.netbox.audition.view.presenter.DetailsDescriptionPresenter
import ir.netbox.audition.viewmodel.MovieViewModel
import ir.netbox.audition.viewmodel.MovieViewModelFactory


/**
 * A wrapper fragment for leanback details screens.
 * It shows a detailed view of video and its metadata.
 */
class MovieDetailsFragment : DetailsSupportFragment() {

    private var mId: Int = 0

    private lateinit var viewModel: MovieViewModel
    private lateinit var factory: MovieViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mId = requireActivity().intent.getIntExtra(DetailsActivity.MOVIE_KEY, 0)
        initViewModel(getService())
        buildDetails()
    }


    private fun initializeBackground(movie: MovieDetail, adapter: ArrayObjectAdapter) {

        val detailsBackground = DetailsSupportFragmentBackgroundController(this)

        detailsBackground.enableParallax()

        Glide.with(requireContext())
            .asBitmap()
            .centerCrop()
            .error(R.drawable.default_background)
            .load(movie.poster)
            .into<SimpleTarget<Bitmap>>(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(
                    bitmap: Bitmap,
                    transition: Transition<in Bitmap>?
                ) {
                    detailsBackground.coverBitmap = bitmap
                    adapter.notifyArrayItemRangeChanged(0, adapter.size())
                }
            })
    }

    private fun buildDetails() {
        val selector = ClassPresenterSelector().apply {
            // Attach your media item details presenter to the row presenter:
            FullWidthDetailsOverviewRowPresenter(DetailsDescriptionPresenter()).also {
                addClassPresenter(DetailsOverviewRow::class.java, it)
                it.backgroundColor =
                    ContextCompat.getColor(requireContext(), R.color.selected_background)
            }
            addClassPresenter(ListRow::class.java, ListRowPresenter())
        }
        val rowsAdapter = ArrayObjectAdapter(selector)

        val width = convertDpToPixel(requireActivity(), DETAIL_THUMB_WIDTH)
        val height = convertDpToPixel(requireActivity(), DETAIL_THUMB_HEIGHT)

        viewModel.getMovie(mId).observe(this, {
            if (it == null) return@observe
            Log.d(TAG, it.toString())

            DetailsOverviewRow(it).apply {

                if (it.hasHD == null || !it.hasHD) {
                    Glide.with(requireContext())
                        .load(it.poster)
                        .centerCrop()
                        .error(R.drawable.app_icon_your_company)
                        .into<SimpleTarget<Drawable>>(object :
                            SimpleTarget<Drawable>(width, height) {
                            override fun onResourceReady(
                                drawable: Drawable,
                                transition: Transition<in Drawable>?
                            ) {
                                imageDrawable = drawable
                                rowsAdapter.notifyArrayItemRangeChanged(0, rowsAdapter.size())
                            }
                        })

                } else {
                    Glide.with(requireContext())
                        .load(it.poster)
                        .centerCrop()
                        .error(R.drawable.app_icon_your_company)
                        .into<SimpleTarget<Drawable>>(object :
                            SimpleTarget<Drawable>(width, height) {
                            override fun onResourceReady(
                                drawable: Drawable,
                                transition: Transition<in Drawable>?
                            ) {
                                imageDrawable =
                                    requireContext().getDrawable(R.drawable.ic_hd)?.let { it1 ->
                                        combineDrawables(
                                            drawable,
                                            it1
                                        ) ?: drawable
                                    }
                                rowsAdapter.notifyArrayItemRangeChanged(0, rowsAdapter.size())
                            }
                        })

                }

                rowsAdapter.add(this)
            }

            initializeBackground(it, rowsAdapter)

        })
        adapter = rowsAdapter
    }

    private fun combineDrawables(img1: Drawable, img2: Drawable): Drawable {
        val finalDrawable = LayerDrawable(arrayOf(img1, img2))
//        finalDrawable.setLayerInsetBottom(0, img2.intrinsicHeight)
        finalDrawable.setLayerGravity(1, Gravity.BOTTOM or Gravity.START)
        return finalDrawable
    }

    private fun convertDpToPixel(context: Context, dp: Int): Int {
        val density = context.applicationContext.resources.displayMetrics.density
        return Math.round(dp.toFloat() * density)
    }

    private fun initViewModel(service: MovieService) {
        factory = MovieViewModelFactory(service)
        viewModel = ViewModelProvider(this, factory)[MovieViewModel::class.java]
    }

    private fun getService(): MovieService {
        val retrofitInstance = RetrofitInstance.getRetrofitInstance()
        return retrofitInstance.create(MovieService::class.java)
    }

    companion object {
        private const val TAG = "VideoDetailsFragment"
        private const val DETAIL_THUMB_WIDTH = 274
        private const val DETAIL_THUMB_HEIGHT = 274

    }
}