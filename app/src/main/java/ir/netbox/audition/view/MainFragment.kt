package ir.netbox.audition.view

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.leanback.app.BackgroundManager
import androidx.leanback.app.BrowseSupportFragment
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import ir.netbox.audition.R
import ir.netbox.audition.data.api.MovieService
import ir.netbox.audition.data.api.RetrofitInstance
import ir.netbox.audition.data.model.ContentX
import ir.netbox.audition.view.presenter.CardPresenter
import ir.netbox.audition.view.presenter.TopicPresenter
import ir.netbox.audition.viewmodel.MovieViewModel
import ir.netbox.audition.viewmodel.MovieViewModelFactory
import java.util.*
import kotlin.collections.ArrayList

/**
 * Loads a list of cards with movies to browse.
 */
class MainFragment : BrowseSupportFragment() {

    private val mHandler = Handler()
    private lateinit var mBackgroundManager: BackgroundManager
    private var mDefaultBackground: Drawable? = null
    private lateinit var mMetrics: DisplayMetrics
    private var mBackgroundTimer: Timer? = null
    private var mBackgroundUri: String? = null


    private lateinit var viewModel: MovieViewModel
    lateinit var factory: MovieViewModelFactory

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initViewModel(getService())

        prepareBackgroundManager()

        setupUIElements()

        loadRows()

        setupEventListeners()
    }

    private fun getService(): MovieService {
        val retrofitInstance = RetrofitInstance.getRetrofitInstance()
        return retrofitInstance.create(MovieService::class.java)
    }

    private fun initViewModel(service: MovieService) {
        factory = MovieViewModelFactory(service)
        viewModel = ViewModelProvider(this, factory)[MovieViewModel::class.java]
    }

    override fun onDestroy() {
        super.onDestroy()
        mBackgroundTimer?.cancel()
    }

    private fun prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(activity)
        mBackgroundManager.attach(requireActivity().window)
        mDefaultBackground =
            ContextCompat.getDrawable(requireActivity(), R.drawable.default_background)
        mMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(mMetrics)
    }

    private fun setupUIElements() {
        title = getString(R.string.browse_title)
        headersState = HEADERS_ENABLED // TODO: Disable it (will cus some problem with live data!)
        searchAffordanceColor = ContextCompat.getColor(requireActivity(), R.color.search_opaque)
        titleView.layoutDirection = View.LAYOUT_DIRECTION_RTL
    }

    private fun loadRows() {

        val rowsAdapter = ArrayObjectAdapter(ListRowPresenter())
        val cardPresenter = CardPresenter()
        val topicPresenter = TopicPresenter()

        val cardsList = ArrayList<ContentX>()
        val topicList = ArrayList<ContentX>()
        val listRowAdapterCards = ArrayObjectAdapter(cardPresenter)
        val listRowAdapterTopics = ArrayObjectAdapter(topicPresenter)

        val listRow1 = ListRow(listRowAdapterCards)
        listRow1.id = 100L

        val listRow2 = ListRow(listRowAdapterTopics)
        listRow2.id = 101L

        viewModel.getMovies().observe(viewLifecycleOwner, { movieData ->
            if (movieData == null) return@observe

            movieData.content?.forEach { content ->

                when (content.type) {
                    "CARDS" -> {
                        if (!content.content.isNullOrEmpty()) {
                            content.content.forEach {
                                cardsList.add(it)
                            }
                        }
                        if (content.showTitle == true) {
                            listRow1.headerItem = HeaderItem(content.headerName)
                        }
                    }
                    "TOPIC" -> {
                        if (!content.content.isNullOrEmpty()) {
                            content.content.forEach {
                                topicList.add(it)
                            }
                        }

                        if (content.showTitle == true) {
                            listRow2.headerItem = HeaderItem(content.headerName)
                        }
                    }


                }
            }


            for (contentX in cardsList) {
                listRowAdapterCards.add(contentX)
            }
            for (contentX in topicList) {
                listRowAdapterTopics.add(contentX)
            }


            rowsAdapter.add(listRow1)
            rowsAdapter.add(listRow2)
        })

        adapter = rowsAdapter

    }

    private fun setupEventListeners() {
        setOnSearchClickedListener {
            Toast.makeText(requireActivity(), "Implement search later!", Toast.LENGTH_LONG)
                .show()
        }
        onItemViewClickedListener = ItemViewClickedListener()
        onItemViewSelectedListener = ItemViewSelectedListener()
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(
            itemViewHolder: Presenter.ViewHolder,
            item: Any,
            rowViewHolder: RowPresenter.ViewHolder,
            row: Row
        ) {

            if (item is ContentX) {
                if (row.id == 101L)
                    return

                val intent = Intent(requireActivity(), DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.MOVIE_KEY, item.iD)

                val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    requireActivity(),
                    (itemViewHolder.view as ImageCardView).mainImageView,
                    DetailsActivity.SHARED_ELEMENT_NAME
                )
                    .toBundle()
                startActivity(intent, bundle)
            }
        }
    }

    private inner class ItemViewSelectedListener : OnItemViewSelectedListener {
        override fun onItemSelected(
            itemViewHolder: Presenter.ViewHolder?, item: Any?,
            rowViewHolder: RowPresenter.ViewHolder, row: Row
        ) {
            if (item is ContentX) {
                mBackgroundUri = item.poster
                startBackgroundTimer()
            }
        }
    }

    private fun updateBackground(uri: String?) {
        val width = mMetrics.widthPixels
        val height = mMetrics.heightPixels
        Glide.with(requireActivity())
            .load(uri)
            .centerCrop()
            .error(mDefaultBackground)
            .into<SimpleTarget<Drawable>>(
                object : SimpleTarget<Drawable>(width, height) {
                    override fun onResourceReady(
                        drawable: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        mBackgroundManager.drawable = drawable
                    }
                })
        mBackgroundTimer?.cancel()
    }

    private fun startBackgroundTimer() {
        mBackgroundTimer?.cancel()
        mBackgroundTimer = Timer()
        mBackgroundTimer?.schedule(UpdateBackgroundTask(), BACKGROUND_UPDATE_DELAY.toLong())
    }

    private inner class UpdateBackgroundTask : TimerTask() {
        override fun run() {
            mHandler.post { updateBackground(mBackgroundUri) }
        }
    }

    companion object {
        private val BACKGROUND_UPDATE_DELAY = 300
    }
}