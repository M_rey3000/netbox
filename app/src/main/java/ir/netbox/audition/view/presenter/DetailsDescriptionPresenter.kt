package ir.netbox.audition.view.presenter

import androidx.leanback.widget.AbstractDetailsDescriptionPresenter
import ir.netbox.audition.data.model.MovieDetail

class DetailsDescriptionPresenter : AbstractDetailsDescriptionPresenter() {

    override fun onBindDescription(
        viewHolder: ViewHolder,
        item: Any
    ) {
        val movie = item as MovieDetail
        viewHolder.apply {
            title.text = movie.title
            subtitle.text = "Rank: ${movie.rank}"
            body.text = "Rate: ${movie.rate}"
        }
    }
}