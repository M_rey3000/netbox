package ir.netbox.audition.data.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ContentX(
    @SerializedName("ID")
    val iD: Int?,
    @SerializedName("Poster")
    val poster: String?,
    @SerializedName("Sort")
    val sort: Int?,
    @SerializedName("Title")
    val title: String?
) : Parcelable