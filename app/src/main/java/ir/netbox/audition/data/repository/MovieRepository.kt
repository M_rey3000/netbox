package ir.netbox.audition.data.repository

import ir.netbox.audition.data.model.MovieData
import ir.netbox.audition.data.model.MovieDetail

interface MovieRepository {
    suspend fun getMovies(): MovieData?
    suspend fun getMovie(id: Int): MovieDetail?
}