package ir.netbox.audition.data.model


import com.google.gson.annotations.SerializedName

data class MovieData(
    @SerializedName("Content")
    val content: List<Content>?,
    @SerializedName("Description")
    val description: String?
)