package ir.netbox.audition.data.repository

import ir.netbox.audition.data.api.MovieService
import ir.netbox.audition.data.model.MovieData
import ir.netbox.audition.data.model.MovieDetail
import retrofit2.HttpException

class MovieRepositoryImpl(private val service: MovieService) : MovieRepository {
    override suspend fun getMovies(): MovieData? {
        var movieData: MovieData? = null
        try {
            val response = service.getMovies()
            val body = response.body()
            if (body != null && response.isSuccessful) {
                movieData = body
            }
        } catch (e: HttpException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return movieData
    }

    override suspend fun getMovie(id: Int): MovieDetail? {
        var movie: MovieDetail? = null
        try {
            val response = service.getMovie(id)
            val body = response.body()
            if (body != null && response.isSuccessful) {
                movie = body
            }
        } catch (e: HttpException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return movie
    }
}