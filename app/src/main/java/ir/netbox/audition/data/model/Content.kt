package ir.netbox.audition.data.model


import com.google.gson.annotations.SerializedName

data class Content(
    @SerializedName("Content")
    val content: List<ContentX>?,
    @SerializedName("HeaderName")
    val headerName: String?,
    @SerializedName("ShowTitle")
    val showTitle: Boolean?,
    @SerializedName("Type")
    val type: String?
)