package ir.netbox.audition.data.api


import com.google.gson.GsonBuilder
import ir.netbox.audition.data.model.MovieData
import ir.netbox.audition.data.model.MovieDetail
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    @GET("/apiv1/media/launcherupdateTEST/MainPage.json")
    suspend fun getMovies(): Response<MovieData>

    @GET("/apiv1/media/launcherupdateTEST/{id}.json")
    suspend fun getMovie(@Path("id") id: Int): Response<MovieDetail>
}

class RetrofitInstance {
    companion object {
        private const val BASE_URL = "https://net-box.ir/"
        fun getRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }
    }
}

