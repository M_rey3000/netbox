package ir.netbox.audition.data.model


import com.google.gson.annotations.SerializedName

data class MovieDetail(
    @SerializedName("hasHD")
    val hasHD: Boolean?,
    @SerializedName("ID")
    val iD: Int?,
    @SerializedName("Poster")
    val poster: String?,
    @SerializedName("Rank")
    val rank: Int?,
    @SerializedName("Rate")
    val rate: Double?,
    @SerializedName("Title")
    val title: String?
)