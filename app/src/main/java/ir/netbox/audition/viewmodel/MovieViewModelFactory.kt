package ir.netbox.audition.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.netbox.audition.data.api.MovieService

class MovieViewModelFactory(private val service: MovieService) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MovieViewModel(service) as T
    }
}