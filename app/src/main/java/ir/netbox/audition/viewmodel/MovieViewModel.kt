package ir.netbox.audition.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import ir.netbox.audition.data.api.MovieService
import ir.netbox.audition.data.repository.MovieRepositoryImpl

class MovieViewModel(service: MovieService) : ViewModel() {
    private val repository = MovieRepositoryImpl(service)
    fun getMovies() = liveData {
        emit(repository.getMovies())
    }

    fun getMovie(id: Int) = liveData {
        emit(repository.getMovie(id))
    }
}